(function ($) {

	// var tabs = $('.page-section__tabs .page-section__tab');
	// var content = $('.page-section__tabs-content');

	// for (var i = 0; i < tabs.length; i++) {
	// 	tabs[i].addEventListener('click', changeTab)
	// }

	// function changeTab() {
	// 	var ind = $(this).index();

	// 	removeClassFrom(tabs, 'active');
	// 	this.classList.add('active');

	// 	TweenMax.to($('.page-section__tabs-content.active'), .3, {
	// 		opacity: 0,
	// 		x: -100,
	// 		onComplete: function () {
	// 			removeClassFrom(content, 'active');
	// 			content[ind].classList.add('active');
	// 			TweenMax.fromTo(content[ind], .5, { x: 100, opacity: 0 }, { x: 0, opacity: 1, ease: Power4.easeOut })
	// 		}
	// 	})
	// }

	// function removeClassFrom(arr, cls) {
	// 	for (var i = 0; i < arr.length; i++) {
	// 		arr[i].classList.remove(cls)
	// 	}
	// }

	/*=========================================================
	=                   Sewing Gallery Tabs                   =
	=========================================================*/

	// var tl = new TimelineMax();
	// $('.tabs-block li').on('click', function () {
	// 	var $label = $('.label');
	// 	var $this = $(this);
	// 	var el_width = $this.width();
	// 	var offset_left = $this.offset();
	// 	var initTabNum = $this.data('menu');
	// 	var $article = $('.page-section__tabs-content');
	// 	var $show = $('.active');
	// 	function step_1() {
	// 		$article.removeClass('active');
	// 	}
	// 	function step_2() {
	// 		$('.num_' + initTabNum).addClass('active');
	// 	}

	// 	if (!tl.isActive()) {
	// 		tl
	// 			.to($article, 0.5, {
	// 				// y: 100,
	// 				autoAlpha: 0,
	// 				ease: Back.easeOut,
	// 				onComplete: step_1
	// 			})
	// 			.fromTo($('.num_' + initTabNum), 0.75, {
	// 				onStart: step_2,
	// 				// y: -100
	// 			},	{
	// 				// y: 0,
	// 				autoAlpha: 1,
	// 				ease: Elastic.easeOut,
	// 				immediateRender: false
	// 			})
	// 		.to($article, 0, {y: 0, ease: Sine.easeOut})
	// 		// $label.offset({
	// 		// 	left: offset_left.left
	// 		// }).css('width', el_width)
	// 		$('.tabs-block li').removeClass('active');
	// 		$this.addClass('active');
	// 	}
	// })

	// var initSize = function () {
	// 	var start_element = $('.tabs-block li:first-of-type');
	// 	var $label = $('.label');
	// 	var initWidth = start_element.css('width')
	// 	$label.css('width', initWidth)
	// }

	// initSize();

	/*============  End of Sewing Gallery Tabs  =============*/

})(jQuery);