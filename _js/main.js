(function ($) {

    /*==================================
    =            Cookie 18+            =
    ==================================*/

    var permissionBlock = $('#permissionBlock'),
        permissionWindow =$('#permissionWindow'),
        permissionClose = $('#permissionClose'),
        permissionText = $('#permissionText'),
        permissionButton = $('#permissionButton'),
        permissionAlert = $('#permissionAlert')
    ;

    function cookieFront() {

        if (!$.cookie('was')) {

            permissionBlock.removeClass('permission--hidden');

        }

        $.cookie('was', true, {
            expires: 7,
            path: '/'
        });

    }

    function permissionFront() {

        permissionButton.on('click', function () {

            function permissioEnter() {

                var tl = new TimelineMax();

                tl
                    .to(permissionWindow, 1.2, {
                        y: '-100%',
                        autoAlpha: 0,
                        ease: Back.easeInOut
                    })
                    .to(permissionBlock, 0.6, {
                        autoAlpha: 0,
                        ease: Power1.easeInOut
                    }, "-=0.6")
                    .set(permissionBlock, {
                        className: "+=permission--hidden"
                    })
                ;

            }
            permissioEnter();

        });

        function permissioAlert() {

            var tl = new TimelineMax({reversed:true});

            tl
                .to(permissionButton, 0.4, {
                    autoAlpha: 0,
                    ease: Power1.easeInOut
                })
                .to(permissionText, 0.4, {
                    autoAlpha: 0,
                    ease: Power1.easeInOut
                }, '-=0.4')
                .to(permissionAlert, 0.4, {
                    autoAlpha: 1,
                    ease: Power1.easeInOut
                })
            ;

            /*jshint -W030 */
            permissionClose.click(function () {
                tl.reversed() ? tl.restart() : tl.reverse();
            });
            return tl;

        }
        permissioAlert();

    }

    /*=====  End of Cookie 18+  ======*/

    $('.wrapper').imagesLoaded({
        background: true
    });

    /*====================================
    =            Front Arrow Down            =
    ====================================*/

    //Arrow down
    var introSection = $('.front-hero'),
        introSectionHeight = introSection.height(),
        //change scaleSpeed if you want to change the speed of the scale effect
        scaleSpeed = 0.3,
        //change opacitySpeed if you want to change the speed of opacity reduction effect
        opacitySpeed = 1.3;
    var arrowDown = $('.arrow-down'),
        opacityArrow = 6;

    //update this value if you change this breakpoint in the style.css file (or _layout.scss if you use SASS)
    var MQ = 480;

    //bind the scale event to window scroll if window width > $MQ (unbind it otherwise)
    function triggerAnimation() {
        if ($(window).width() >= MQ) {
            $(window).on('scroll', function () {
                //The window.requestAnimationFrame() method tells the browser that you wish to perform an animation- the browser can optimize it so animations will be smoother
                window.requestAnimationFrame(animateIntro);
            });
        } else {
            $(window).off('scroll');
        }
    }

    triggerAnimation();
    $(window).on('resize', function () {
        triggerAnimation();
    });

    //assign a scale transformation to the introSection element and reduce its opacity
    function animateIntro() {
        var scrollPercentage = ($(window).scrollTop() / introSectionHeight).toFixed(5),
            scaleValue = 1 - scrollPercentage * scaleSpeed;
        //check if the introSection is still visible
        if ($(window).scrollTop() < introSectionHeight) {
            arrowDown.css({ 'opacity': 1 - scrollPercentage * opacityArrow });
        }
        arrowDown.delay(600).fadeOut(600);
    }

    /*=====  End Arrow Down  ======*/

    /*=================================
    =            Portfolio            =
    =================================*/

    function projectsGridder() {
        $('#newsGridder').gridderExpander({
            scroll: true,
            scrollOffset: 168,
            scrollTo: "panel", // panel or listitem
            animationSpeed: 900,
            animationEasing: "easeInOutExpo",
            showNav: true, // Show Navigation
            nextText: "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 100 200\"><title>bm_next</title><polyline points=\"0 0 100 100 0 200\"/></svg>",
            prevText: "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 100 200\"><title>bm_prev</title><polyline points=\"100 200 0 100 100 0\"/></svg>",
            closeText: "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 200.71 200.71\"><title>bm_close</title><line x1=\"200\" y1=\"200\" x2=\"0\" y2=\"0\"/><line x1=\"200\" y1=\"0\" x2=\"0\" y2=\"200\"/></svg>"
        });
    }

    /*---------  Portfolio Mobile  ---------*/
    function projectsGridderMobile() {
        $('#newsGridder').gridderExpander({
            scroll: true,
            scrollOffset: 80,
            scrollTo: "panel", // panel or listitem
            animationSpeed: 900,
            animationEasing: "easeInOutExpo",
            showNav: true, // Show Navigation
            nextText: "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 100 200\"><title>bm_next</title><polyline points=\"0 0 100 100 0 200\"/></svg>",
            prevText: "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 100 200\"><title>bm_prev</title><polyline points=\"100 200 0 100 100 0\"/></svg>",
            closeText: "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 200.71 200.71\"><title>bm_close</title><line x1=\"200\" y1=\"200\" x2=\"0\" y2=\"0\"/><line x1=\"200\" y1=\"0\" x2=\"0\" y2=\"200\"/></svg>"
        });
    }

    /*=====  End of Portfolio  ======*/

    /*================================
    =            Delivery            =
    ================================*/

    function hideaddress() {
        if ($('[name="delivery"]:checked').attr('id') == 'delivery_1') {
            $("#adressDelivery").hide();
        } else {
            $("#adressDelivery").show();
            return false;
        }
    }

    window.onload = function () {
        hideaddress();
    };

    $(document).on('change', '[name="delivery"]', function () {
        hideaddress();
    });

    /*=====  End of Delivery  ======*/


    /*====================================
    =            Front Slider            =
    ====================================*/

    // var sliderFront = new MasterSlider();
    // sliderFront.control('arrows' ,{insertTo:'#slider_arrow_front',autohide:false});
    // // sliderFront.control('slideinfo',{insertTo:'#[[+galleryScriptInfo]]'});

    // sliderFront.setup('news-slider' , {
    //     width: 730,
    //     height: 880,
    //     autoHeight: false,
    //     space: 0,
    //     loop: true,
    //     view:'flow',
    //     layout:'autofill', //fullscreen
    //     speed:20,
    //     grabCursor: true,
    //     swipe: true,
    //     mouse: true
    // });

    /*=====  End of Front Slider  ======*/

    /*=====  End of Menu  ======*/

    /*===================================
    =            Menu Mobile            =
    ===================================*/

    var navButtonMobile = $('#navButtonMobile'),
        navMainMobile = $('.header__nav--mobile'),
        navMainListMobile = $('.nav-mobile__menu li'),
        navBack = $('.nav-mobile__mask'),
        navButtonLineTop = $('.nav-button-line__top'),
        navButtonLineMiddle = $('.nav-button-line__middle'),
        navButtonLineBottom = $('.nav-button-line__bottom'),
        navWrapper = $('.wrapper'),
        navHeaderTop = $('.header-top'),
        headerBack = $('#headerBack')
     ;

    function navMenuOpenMobile() {

        var tl = new TimelineMax({
            paused: true,
            reversed: true
        });

        tl
            .to(headerBack, 0.4, {
                // zIndex: 8999,
                autoAlpha: 1,
                visibility: 'visible',
                // xPercent: -100,
                ease: Power1.easeInOut
            }, "-=1")
            .to(navMainMobile, 0.4, {
                xPercent: '-100%',
                // zIndex: 2,
                // autoAlpha: 1,
                ease: Power2.easeInOut
            }, "-=1.2")
            // .to(navWrapper, 0.6, {
            // 	xPercent: '50%',
            // 	autoAlpha: 0,
            // 	ease: Power2.easeInOut
            // }, "-=0.6")
            // .to(navHeaderTop, 0.6, {
            // 	yPercent: '-100%',
            // 	ease: Power2.easeInOut
            // }, "-=0.6")
            // .to(navButtonLineMiddle, 0.3, {
            //     rotation: '180deg',
            //     ease: Power2.easeInOut
            // }, "-=0.8")
            // .to(navButtonLineTop, 0.3, {
            //     rotation: '135deg',
            //     xPercent: 30,
            //     yPercent: 450,
            //     scaleX: 0.6,
            //     ease: Power2.easeInOut
            // }, "-=0.6")
            // .to(navButtonLineBottom, 0.3, {
            //     rotation: '-135deg',
            //     xPercent: 30,
            //     yPercent: -450,
            //     scaleX: 0.6,
            //     ease: Power2.easeInOut
            // }, "-=0.6")
            // .staggerFrom(navMainListMobile, 0.6, {
            //     yPercent: '30%',
            //     autoAlpha: 0,
            //     ease: Back.easeInOut
            // }, "0.07", "-=0.3")
        ;

    /*jshint -W030 */
        navButtonMobile.on('click', function () {
            tl.reversed() ? tl.restart() : tl.reverse();
        });

        headerBack.on('click', function () {
            tl.reverse(-0.2);
        });

    }

    $('.item-has-children').children('a').on('click', function (event) {
        event.preventDefault();
        $(this).toggleClass('submenu-open').next('.submenu').slideToggle(200).end().parent('.item-has-children').siblings('.item-has-children').children('a').removeClass('submenu-open').next('.submenu').slideUp(200);
    });

    /*=====  End of Menu Mobile  ======*/

    /*==============================
    =            Search            =
    ==============================*/

    var searchToggle = $("#headerSearch"),
        searchBlock = $("#searchBlock"),
        searchBack = $("#searchBack"),
        searchClose = $(".search-close"),
        searchForm = $("#mse2_form"),
        searchIcon = $("#searchIcon"),
        searchBottle = $("#searchBottle"),
        searchMagnifier = $("#searchMagnifier"),
        searchCross = $("#searchCross"),
        searchIconOver = $("#searchIconOver"),
        searchBlockHelp = $('#searchBlockHelp'),
        searchMobileClose = $("#searchMobileClose"),
        searchBlockMobile = $("#searchBlockMobile"),
        searchToggleMobile = $("#searchToggleMobile"),
        searchHeader = $('.header')
    ;

    function searchBlockActive() {

        var tl = new TimelineMax({reversed:true, paused:true});

        tl
            // .to(searchHeader, 0, {
            //     boxShadow: "none"
            // }, "-=0.4")
            .to(searchBlock, 0.5, {
                xPercent: "-20%",
                autoAlpha: 1,
                // zIndex: 9001,
                ease: Cubic.easeInOut
            }, "-=0.7")
        ;

        /*jshint -W030 */
        searchToggle.on("click", function () {
            tl.reversed() ? tl.restart() : tl.reverse(-0.2);
        });
        // searchClose.on("click", function () {
        //     tl.reversed() ? tl.restart() : tl.reverse();
        // });

        return tl;
    }

    /*=====  End of Search  ======*/

    /*=====================================
    =            Filter Mobile            =
    =====================================*/

    var filterMobile = $('#filterMobile'),
        categoryFilter = $('.sidebar'),
        categoryGrid = $('.category-grid'),
        vendorDescription = $('.vendor-description'),
        vendorDescriptionText = $('.vendor-description__text'),
        filterBackMobile = $('#filterBackMobile'),
        headerTopMobile = $('#header'),
        footerMobile = $('.footer'),
        categoryFilterClose = $('.category-filter__close'),
        filterBack = $('#filterBack')
    ;

    function filterCategoryMobile() {

        var tl = new TimelineMax({reversed:true, paused:true});

        tl
            .to([headerTopMobile, footerMobile], 0.3, {
                autoAlpha: 0,
                ease: Power1.easeInOut},
                "-=1.6")
            .to(filterBack, 0.4, {
                // zIndex: 8999,
                autoAlpha: 1,
                visibility: 'visible',
                // xPercent: -100,
                ease: Power1.easeInOut
            }, "-=1")
            .to(categoryFilter, 0.5, {
                xPercent: "-100%",
                zIndex: 9999,
                ease: Power2.easeInOut},
                "-=0.8")
            .to(filterBackMobile, 0.4, {
                // zIndex: 9100,
                autoAlpha: 0.5,
                ease: Power1.easeInOut},
                "-=0.15")
            // .from(categoryFilterClose, 0.4, {
            //     autoAlpha: "0",
            //     ease: Power1.easeInOut})
        ;

        /*jshint -W030 */
        filterMobile.click(function () {
            tl.reversed() ? tl.restart() : tl.reverse();
        });
        filterBackMobile.click(function () {
            tl.reverse();
        });
        // categoryFilterClose.click(function () {
        //     tl.reverse();
        // });
        filterBack.on('click', function () {
            tl.reverse(-0.2);
        });

        return tl;
    }

    /*=====  End of Filter Mobile  ======*/

    /*================================
    =            MiniCart            =
    ================================*/

    const miniCartToggle = $('.mini-cart'),
        miniCartContent = $('#mcd-mini-cart'),
        miniCartBack = $('#cart_back_svg'),
        miniCartButton = $('#dropdownMiniCart'),
        minicartIcon = $('#minicartIcon'),
        msMiniCart = $('#msMiniCart'),
        miniCartClose = $('.minicart-close'),
        miniCartLink = $('#miniCartLink'),
        minicartFull = $('#minicartFull'),
        minicartFilter = $('.category-filter'),
        cartButton = $('.minicart__button'),
        productContentSubmitCart = $('.product-content__submit-cart')
    ;

    function miniCartRight() {

        let tl = new TimelineMax({reversed:true, paused:true});

        tl
            .to(miniCartContent, 0.5, {
                xPercent: -100,
                // zIndex: 100,
                ease: Cubic.easeInOut
            }, "-=0.6")
            .to(cartButton, 0.6, {
                xPercent: "-20%",
                autoAlpha: 1,
                display: "flex",
                ease: Back.easeInOut
            }, "-=0.4")
            // .to(minicartFilter, 0.6, {
            //     autoAlpha: "0",
            //     ease: Power1.easeInOut
            // }, "-=0.6")
            // .to(miniCartLink, 0.6, {
            //     display: "flex",
            //     ease: Power1.easeInOut
            // }, "-=0.6")
            // .to(miniCartIcon, 0.4, {
            //     autoAlpha: "0",
            //     ease: Back.easeInOut
            // }, "-=0.8")
            // .to(miniCartCount, 0.4, {
            //     autoAlpha: "0",
            //     ease: Back.easeInOut
            // }, "-=0.6")
            // .to(miniCartClose, 0.4, {
            //     autoAlpha: "1",
            //     zIndex: "2",
            //     ease: Back.easeInOut
            // }, "-=0.4")
            // .to(miniCartLink, 1, {
            //     autoAlpha: "1",
            //     x: "-50%",
            //     ease: Back.easeInOut
            // }, "-=0.8")
        ;

        /*jshint -W030 */
        miniCartButton.on("click", function () {
            tl.reversed() ? tl.restart() : tl.reverse();
        });
        minicartIcon.on("click", function () {
            tl.reversed() ? tl.restart() : tl.reverse();
        });
        // miniCartClose.on("click", function () {
        //     tl.reversed() ? tl.restart() : tl.reverse();
        // });

        return tl;

    }

    /*=====  End of MiniCart  ======*/

    /*================================
    =            MiniCart Mobile            =
    ================================*/

    const minicartActive = $('.minicart__button')
    ;

    function miniCartMobile() {

        let tl = new TimelineMax({reversed:true, paused:true});

        tl
            .to(filterMobile, 0.3, {
                autoAlpha: 0,
                yPercent: 100,
                ease: Power2.easeInOut
            }, "-=1")
            .to(miniCartContent, 0.5, {
                xPercent: -100,
                // zIndex: 100,
                ease: Power2.easeInOut
            }, "-=0.6")
            // .to(miniCartButton, 0.6, {
            //     // xPercent: "-20%",
            //     autoAlpha: 1,
            //     display: "flex",
            //     ease: Power2.easeInOut
            // }, "-=0.4")
            .to(minicartActive, 0.4, {
                xPercent: -100,
                autoAlpha: 1,
                display: "flex",
                ease: Power1.easeInOut
            }, "-=0.4")
        ;

        /*jshint -W030 */
        miniCartButton.on("click", function () {
            tl.reversed() ? tl.restart() : tl.reverse();
        });

        return tl;

    }

    /*=====  End of MiniCart Mobile  ======*/

    /*======================================
    =            InfoNav Mobile            =
    ======================================*/

    var infoSidebar = $('#infoSidebar'),
        infoNavMobile = $('#infoNavMobile'),
        infoContent = $('#infoContent'),
        infoActiveLink = $('#infoSidebar ul li'),
        infoNavClose = $('#infoNavClose')
    ;

    function infoContentMobile() {

        var tl = new TimelineMax({reversed:true, paused:true});

        tl
            .to(infoSidebar, 0.6, {
                right: 0,
                zIndex: 9900,
                ease: Power1.easeInOut},
                "-=0.4")
            // .to(infoNavMobile, 0.3, {
            //     autoAlpha: "0",
            //     display: "none",
            //     ease: Power1.easeInOut},
            //     "-=0.6")
            .from(infoNavClose, 0.4, {
                autoAlpha: "0",
                ease: Power1.easeInOut})
            // .to([searchBlockMobile, infoNavMobile, headerLogoMobile, navToggleMobile, headerTopMobile, headerBottomMobile, headerLeftMobile, headerRightMobile], 0.3, {
            //     autoAlpha: "0",
            //     display: "none",
            //     ease: Power1.easeInOut},
            //     "-=0.6")

        ;

        /*jshint -W030 */
        infoNavMobile.click(function () {
            tl.reversed() ? tl.restart() : tl.reverse();
        });
        infoActiveLink.click(function () {
            tl.reversed() ? tl.restart() : tl.reverse();
        });
        infoNavClose.click(function () {
            tl.reverse();
        });

        return tl;
    }

    /*=====  End of InfoNav Mobile  ======*/


    /*=============================
    =            Input            =
    =============================*/

    function itemCounter(field){

        var fieldCount = function(el) {

            var
                // Мин. значение
                min = el.data('min') || false,

                // Макс. значение
                max = el.data('max') || false,

                // Кнопка уменьшения кол-ва
                dec = el.prev('.dec'),

                // Кнопка увеличения кол-ва
                inc = el.next('.inc');

            function init(el) {
                if(!el.attr('disabled')){
                    dec.on('click', decrement);
                    inc.on('click', increment);
                }

                // Уменьшим значение
                function decrement() {
                    var value = parseInt(el[0].value);
                    value--;

                    if(!min || value >= min) {
                        el[0].value = value;
                    }
                }

                // Увеличим значение
                function increment() {
                    var value = parseInt(el[0].value);

                    value++;

                    if(!max || value <= max) {
                        el[0].value = value++;
                    }
                }

            }

            el.each(function() {
                init($(this));
            });
        };

        $(field).each(function(){
            fieldCount($(this));
        });
    }

    function cartCounter() {
        $('.cart-table__count-down').click(function () {
            var $input = $(this).parent().find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        $('.cart-table__count-up').click(function () {
            var $input = $(this).parent().find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });
    }

    // /*=====  End of Input  ======*/

    function copyrightReplace() {
        $('.footer-container__copyright p, .front-hero ul>li>a').find('br').replaceWith('&nbsp;');
    }

    var categoryWrapperDesktop = $('.wrapper-category--desktop'), categoryWrapperMobile = $('.wrapper-category--mobile');
    function categoryDesktopRemove() {
        categoryWrapperDesktop.remove();
    }

    function categoryMobileRemove() {
        categoryWrapperMobile.remove();
    }

    /*==================================================
    =                   News Gallery                   =
    ==================================================*/

    function newsBXSlider() {
        $(".news-full__gallery").bxSlider({
            'wrapperClass': 'news-full__gallery',
            'mode': 'horizontal',
            // 'auto': true,
            // 'pause': 8000,
            'slideMargin': 32,
            'pager': false,
            'touchEnabled': false,
            'autoHover': false
        });
    }

    /*============  End of News Gallery  =============*/

    function initPage() {
        permissionFront();
        cookieFront();
        searchBlockActive();
        miniCartRight();
        navMenuOpenMobile();
        itemCounter('#product_price');
        cartCounter();
        // projectsGridder();
        categoryMobileRemove();
        newsBXSlider();
    }

    function initPageMobile() {
        navMenuOpenMobile();
        searchBlockActive();
        filterCategoryMobile();
        itemCounter('#product_price');
        cartCounter();
        copyrightReplace();
        // projectsGridderMobile();
        miniCartMobile();
        categoryDesktopRemove();
    }

    if (document.body.clientWidth > 768 || screen.width > 768) {

        initPage();

    } else {

        initPageMobile();
    }

})(jQuery);