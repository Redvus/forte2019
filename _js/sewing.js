(function ($) {

	/*----------  Input mask  ----------*/
	$("[name = name]").inputmask("a{1,64}");
	$("[name = phone]").inputmask({mask: "+7 (999) 999 99 99"});
	$("#af_count").inputmask({mask: "9{1,3}"});
	$("[name = email]").inputmask({
		mask: "*{1,64}@a{1,64}[.a{1,3}]",
		definitions: {
		"*": {
			validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~-]",
			cardinality: 1,
			casing: "lower"
		}
		}
	});


})(jQuery);